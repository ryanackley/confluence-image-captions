AJS.toInit(function(){
	var $ = AJS.$, 
		useCaptionRegex=/(^|\&)(captionId\=([^&]+))($|&)/;

	var setCaptionWidth = function(img, $captionDiv, $wrapper){
		$captionDiv.css('width', img.clientWidth + 'px');
		if ($wrapper){
			$wrapper.css('width', img.clientWidth + 'px');
		}
	}
	$('img.confluence-embedded-image[title]').each(function(){
		var $img = $(this);
		var queryParams = $img.attr('confluence-query-params');

		if (queryParams) {
			var match = useCaptionRegex.exec(queryParams);
			if (match) {
				var id = match[3];
				var text = $img.attr('title');

				

				var $parent = $img.parent(), 
					$wrapper;

				if (!$parent.hasClass('confluence-embedded-file-wrapper')){
					$wrapper = $('<span></span>');
					$wrapper.css({'display' : 'inline-block'});
					if ($img.hasClass('image-left')){
						$wrapper.addClass('image-left');
						$img.removeClass('image-left');
					}
					else if ($img.hasClass('image-center')){
						$wrapper.addClass('confluence-embedded-image');
						$wrapper.addClass('image-center');
						$img.removeClass('image-center');
						$wrapper.css({'display' : ''});
					}
					else if ($img.hasClass('image-right')){
						$wrapper.addClass('image-right');
						$img.removeClass('image-right');
					}
					$img.css({'display' : 'block'});
					$img.wrap($wrapper);
					$parent = $img.parent();
					$wrapper = $parent;
				}
				var $captionDiv = $(Screenshout.Captions.Templates.captionView({text: text, anchorId: id}));
				if ($wrapper){
					$captionDiv.css({"margin" : "0px"});
				}
				$img.after($captionDiv);

				$parent.addClass('confluence-caption-wrapper');
				if (this.complete && this.naturalWidth != 0) {
					setCaptionWidth(this, $captionDiv, $wrapper);
				}
				else{
					$img.on('load', setCaptionWidth.bind(null, this, $captionDiv, $wrapper));
				}

			}
		}
	});
});