AJS.toInit(function(){
	var currentImg,
		useCaptionRegex=/(^|\&)(captionId\=([^&]+))($|&)/;

	AJS.bind('dialog-created.image-properties', function(event, data) {
		currentImg = data.img;
	});
	AJS.bind('dialog-before-show.image-properties', function() {
		var $ = AJS.$;

		var $img = $(currentImg);
		var queryParams = $img.attr('confluence-query-params');
		var match = useCaptionRegex.exec(queryParams);
		$('.image-attributes-panel form.aui').append(Screenshout.Captions.Templates.captionCheckbox({checked: !!match}));
		$('#image-properties-dialog .button-panel-submit-button').on('mousedown', function(){
			try{
				var useCaptionStr = ''
				if ($('#use-caption-checkbox:checked').length > 0){
					useCaptionStr = 'captionId=' + $img.attr('data-linked-resource-id');
				}

				if (queryParams){
					
					if (match){
						queryParams = queryParams.replace(match[2], useCaptionStr);
					}
					else {
						queryParams = queryParams + '&' + useCaptionStr;
					}
				}
				else{
					queryParams = useCaptionStr;
				}
				$img.attr('confluence-query-params', queryParams);
			}
			catch(e){}
		});
	});
});